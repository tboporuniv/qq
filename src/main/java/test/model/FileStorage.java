package test.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "documents")
public class FileStorage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "upload_id")
    @NotNull
    @Getter
    private Long upload_id;

    //     .jpg, .png, .jpeg, .doc, .docx, .pdf, .xlsx
    @Column(name = "file_name")
    @Setter
    @Getter
    private String fileName = null;

    @Column(name = "file_download_uri")
    @Setter
    @Getter
    private String fileDownloadUri;

    @Column(name = "file_type")
    @Setter
    @Getter
    private String fileType = null;

    @Column(name = "size")
    @Setter
    @Getter
    private long size;

//    TODO: user ownership
//    @Column(name = "user_login")
//    @Getter
//    @Setter
//    private String user_login;

    public FileStorage(String fileName, String fileDownloadUri, String fileType, long size) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }

}